﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections;

namespace Average_Speed
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string line;
            StreamReader file;
            DialogResult window = openFileDialog1.ShowDialog();
            Class1 myObject = new Class1();
            ArrayList cameras = new ArrayList();
            ArrayList cartimes = new ArrayList();
            if (window == DialogResult.OK)
            {
                string input = openFileDialog1.FileName;
                try
                {
                    file = new StreamReader(input);
                    while ((line = file.ReadLine()) != null)
                    {
                        Match speedlimit = Regex.Match(line, @"Speed limit is ([A-Za-z0-9\-]+)");
                        Match cameradistance = Regex.Match(line, @"Speed camera number ([A-Za-z0-9\-]+) is ([A-Za-z0-9\-]+)");
                        Match times = Regex.Match(line, @"Vehicle ([A-Za-z0-9\-]+) ([A-Za-z0-9\-]+) passed camera ([A-Za-z0-9\-]+) at ([A-Za-z0-9\-]+).");
                        //IF control for speed limit
                        if(speedlimit.Success)
                        {
                            Match speedmatch = Regex.Match(line, @"\d+");
                            myObject.setSpeedLimit(speedmatch.Value);
                        }
                        //IF control for Camera Distance
                        if (cameradistance.Success)
                        {
                            cameras.Add(cameradistance.Value);
                        }
                        //IF control for Times
                        if (times.Success)
                        {
                            cartimes.Add(times.Value);
                        }
                    }
                    myObject.setCameras(cameras);
                    myObject.setTimes(cartimes);

                    textBox1.Refresh();
                    file.Close();
                }
                catch (IOException)
                {
                }
            }
        }
    }
}
