﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System;
using System.Collections;
using System.Text.RegularExpressions; 

namespace Average_Speed
{
    class Class1
    {
        public Class1()
        {
            cameralist = new ArrayList();
            distancelist = new ArrayList();
            timeslist = new ArrayList();
            licenselist = new ArrayList();
        }

        public void setSpeedLimit(string limit)
        {
            speedlimit = Convert.ToDouble(limit);
        }

        public void setCameras(ArrayList c)
        {
            try
            {
                int i = 0;
                
                while (i<c.Count)
                {
                    string value = c[i].ToString();
                    //could give error: \d+
                    Match cameramatch = Regex.Match(value, @"\d");
                    cameralist.Add(Convert.ToDouble(cameramatch.Value));
                    cameramatch = cameramatch.NextMatch();
                    distancelist.Add(Convert.ToDouble(cameramatch.Value));
                    i++;
                }
                if (Convert.ToDouble(cameralist[0]) != 0 || Convert.ToDouble(cameralist[1]) < Convert.ToDouble(cameralist[0]) || Convert.ToDouble(cameralist[2]) < Convert.ToDouble(cameralist[1])
                    || Convert.ToDouble(cameralist[3]) < Convert.ToDouble(cameralist[2]) || Convert.ToDouble(cameralist[0]) < 0 || Convert.ToDouble(cameralist[1]) < 0
                    || Convert.ToDouble(cameralist[2]) < 0 || Convert.ToDouble(cameralist[3]) < 0)
                {
                    throw new CameraIsWrong();
                }
            }
            catch (CameraIsWrong e)
            {
                //put exception here
            }
        }

        public void setTimes(ArrayList t)
        {
            int i = 0;
            while (i < t.Count)
            {
                string value = t[i].ToString();
                Match timesmatch = Regex.Match(value, @"Vehicle ([A-Za-z0-9\-]+) ([A-Za-z0-9\-]+)");
                licenselist.Add(timesmatch.Value);

            }
        }


        double speedlimit;
        ArrayList cameralist;
        ArrayList distancelist;
        ArrayList timeslist;
        ArrayList licenselist;
    }

    class CameraIsWrong : Exception
    {
        public override string Message
        {
            get
            {
                return "Invalid Input";
            }
        }
    }
}
